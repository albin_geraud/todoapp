import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TitleReducerService {
  private readonly _title = new BehaviorSubject<any>('Add To Do');
  readonly  title$ = this._title.asObservable();

  constructor() { }
  get title(): any {
    return this._title.getValue();
  }
  set title(newTitle) {
     this._title.next(newTitle);
  }
  async changeTitle(oldTitle, payload) {
    this.title = payload;
  }
}
