import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {TodoListReducerService} from './todo-list-reducer.service';
import {TitleReducerService} from './title-reducer.service';
import {combineLatest} from 'rxjs/internal/observable/combineLatest';

@Injectable({
  providedIn: 'root'
})
export class AppStoreService {
  private readonly _appState = new BehaviorSubject<any>({
    title: 'Add To Do',
    todoList: []
  });
  readonly appState$ = this._appState.asObservable();

  constructor(private todolistservice: TodoListReducerService, private titleservice: TitleReducerService) {
    combineLatest(todolistservice.todolist$, titleservice.title$)
      .subscribe(([todolist, Title]) => {
          this.appState = {
            title: Title,
            todoList: todolist
          };
      });
  }
  get appState(): any {
    return this._appState.getValue();
  }

  set appState(newState) {
    this._appState.next(newState);
  }
  async addTodo(payload) {
    this.todolistservice.addTodo(this.appState.todoList, payload);

  }
  async removeLastTodo() {
    this.todolistservice.removeLastTodo(this.appState.todoList);

  }
  async changeTitle(payload) {
    this.titleservice.changeTitle(this.appState.title, payload);

  }
}
