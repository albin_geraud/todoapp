import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoListReducerService {
  private readonly _todoList = new BehaviorSubject<any>([]);
  readonly todolist$ = this._todoList.asObservable();

  constructor() {
  }

  get todoList(): any {
    return this._todoList.getValue();
  }

  set todoList(newList) {
    this._todoList.next(newList);
  }

  async addTodo(oldlist, payload) {
    this.todoList = [...this.todoList, payload];

  }

  async removeLastTodo(oldlist) {
    const newlist = oldlist
    newlist.pop();
    this.todoList = newlist;
  }
}
