import {Component} from '@angular/core';
import {AppStoreService} from './services/app-store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'todoApp';
  value;
  state$;
  todoList: any[] = ['Pepper', 'Salt', 'Papikra'];

  constructor(private appstore: AppStoreService) {
    this.state$ = this.appstore.appState$;
  }

  addTodo() {
    this.appstore.addTodo(this.value);
  }

  removeLastTodo() {
    this.appstore.removeLastTodo();
  }
}


